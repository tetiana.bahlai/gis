import shapefile
from json import dumps


def main():
    reader = shapefile.Reader("UKR_roads.shp", "UKR_roads.dbf")
    fields = reader.fields[1:]
    field_names = [field[0] for field in fields]
    buffer = []
    for sr in reader.shapeRecords():
        atr = dict(zip(field_names, sr.record))
        geom = sr.shape.__geo_interface__
        buffer.append(dict(type="Feature", geometry=geom, properties=atr))

    geojson = open("ukraine_roads.geojson", "w")
    geojson.write(dumps({"type": "FeatureCollection", "features": buffer}, indent=2) + "\n")
    geojson.close()


if __name__ == '__main__':
    main()
